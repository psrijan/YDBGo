//////////////////////////////////////////////////////////////////
//								//
// Copyright (c) 2018-2019 YottaDB LLC and/or its subsidiaries.	//
// All rights reserved.						//
//								//
//	This source code contains the intellectual property	//
//	of its copyright holder(s), and is made available	//
//	under a license.  If you do not know the terms of	//
//	the license, please stop and do not read further.	//
//								//
//////////////////////////////////////////////////////////////////

package yottadb

import (
	"fmt"
	"runtime"
	"strings"
	"unsafe"
)

// #include "libyottadb.h"
import "C"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Miscellaneous functions
//
////////////////////////////////////////////////////////////////////////////////////////////////////

// max is a function to provide max integer value between two given values.
func max(x int, y int) int {
	if x >= y {
		return x
	}
	return y
}

// printEntry is a function to print the entry point of the function, when entered, if the printEPHdrs flag is enabled.
func printEntry(funcName string) {
	if dbgPrintEPHdrs {
		_, file, line, ok := runtime.Caller(2)
		if ok {
			fmt.Println("Entered ", funcName, " from ", file, " at line ", line)
		} else {
			fmt.Println("Entered ", funcName)
		}
	}
}

// initkey is a function to initialize a provided key with the provided varname and subscript array in string form.
func initkey(tptoken uint64, errstr *BufferT, dbkey *KeyT, varname string, subary []string) {
	var maxsublen, sublen, i uint32
	var err error

	subcnt := uint32(len(subary))
	maxsublen = 0
	for i = 0; i < subcnt; i++ {
		// Find maximum length of subscript so know how much to allocate
		sublen = uint32(len(subary[i]))
		if sublen > maxsublen {
			maxsublen = sublen
		}
	}
	dbkey.Alloc(uint32(len(varname)), subcnt, maxsublen)
	dbkey.Varnm.SetValStr(tptoken, errstr, varname)
	if nil != err {
		panic(fmt.Sprintf("YDB: Unexpected error with SetValStr(): %s", err))
	}
	// Load subscripts into KeyT (if any)
	for i = 0; i < subcnt; i++ {
		err = dbkey.Subary.SetValStr(tptoken, errstr, i, subary[i])
		if nil != err {
			panic(fmt.Sprintf("YDB: Unexpected error with SetValStr(): %s", err))
		}
	}
	err = dbkey.Subary.SetElemUsed(tptoken, errstr, subcnt)
	if nil != err {
		panic(fmt.Sprintf("YDB: Unexpected error with SetUsed(): %s", err))
	}
}

// allocMem is a function to allocate memory optionally initializing it in various ways. This can be a future
// point where storage management sanity code can be added.
func allocMem(size C.size_t) unsafe.Pointer {
	// This initial call must be to calloc() to get initialized (cleared) storage. We cannot allocate it and then
	// do another call to initialize it as that means uninitialized memory is traversing the cgo boundary which
	// is what triggers the cgo bug mentioned in the cgo docs (https://golang.org/cmd/cgo/#hdr-Passing_pointers).
	mem := C.calloc(1, size)
	if dbgInitMalloc && (0x00 != dbgInitMallocChar) { // Want to initialize to something other than nulls
		_ = C.memset(mem, dbgInitMallocChar, size)
	}
	return mem
}

// freeMem is a function to return memory allocated with allocMem() or C.calloc().
func freeMem(mem unsafe.Pointer, size C.size_t) {
	if dbgInitFree {
		_ = C.memset(mem, dbgInitFreeChar, size)
	}
	C.free(mem)
}

// errorFormat is a function to replace the FAO codes in YDB error messages with meaningful data. This is normally
// handled by YDB itself but when this Go wrapper raises the same errors, no substitution is done. This routine can
// provide that substitution. It takes set of FAO-code and value pairs performing those substitutions on the error
// message in the order specified. Care must be taken to specify them in the order they appear in the message or
// unexpected substitutions may occur.
func errorFormat(errmsg string, subparms ...string) string {
	if 0 != (uint32(len(subparms)) & 1) {
		panic("YDB: Odd number of substitution parms - invalid FAO code and substitution value pairing")
	}
	for i := 0; i < len(subparms); i = i + 2 {
		errmsg = strings.Replace(errmsg, subparms[i], subparms[i+1], 1)
	}
	return errmsg
}

// formatINVSTRLEN is a function to do the fetching and formatting of the INVSTRLEN error with both of its
// substitutable parms filled in.
func formatINVSTRLEN(tptoken uint64, errstr *BufferT, lenalloc, lenused C.uint) string {
	errmsg, err := MessageT(tptoken, errstr, (int)(YDB_ERR_INVSTRLEN))
	if nil != err {
		panic(fmt.Sprintf("YDB: Error fetching INVSTRLEN: %s", err))
	}
	errmsg = errorFormat(errmsg, "!UL", fmt.Sprintf("%d", lenused), "!UL", fmt.Sprintf("%d", lenalloc)) // Substitute parms
	return errmsg
}

// IsLittleEndian is a function to determine endianness. Exposed in case anyone else wants to know.
func IsLittleEndian() bool {
	var bittest = 0x01

	if 0x01 == *(*byte)(unsafe.Pointer(&bittest)) {
		return true
	}
	return false
}

// Exit is a function to drive YDB's exit handler in case of panic or other non-normal shutdown that bypasses
// atexit() that would normally drive the exit handler.
func Exit() {
	rc := C.ydb_exit()
	if YDB_OK != rc {
		panic(fmt.Sprintf("YDB: Exit() got return code %d from C.ydb_exit()", rc))
	}
}
